# gridgenmap.py
#
# A separate function of GridgenArbitraryAirfoil just for mapping. this
# returns a cell array (fcell) as the combinataion of the SC exterior map 
# object (f), foilefile name (foilfile), and outer radius (OR) for the 
# later use of Gridgenmesh.m. The mapping file can be saved in .mat file.
# Type either 'T' or 't' as the third variable if you save in .mat file.

import os
import sys
import time
import numpy as np 
import scipy.io as sio

def gridgenmap(foifile,OR,save_in_mat=None):

    start = time.time() # Get initial time for record keeping

    # Set the geometry of the foil
    fname1 = '../inputs/airfoils/coords/' + foifile + '.txt'

    # Stores the geometry of the foil
    dirname2 = '../inputs/airfoils/mapping_files/'
    fname2 = dirname2 + foifile + '_OR' + str(OR) + '.npy'
    fname3 = dirname2 + foifile + '_OR' + str(OR) + '.mat'
    os.makedirs(dirname2, exist_ok=True)
    # Check if there is either intended .npy or .mat file to read
    if os.path.exists(fname2):
        gridgen_quick_n = True
    elif os.path.exists(fname3):
        gridgen_quick_m = True
    else:
        gridgen_quick_n = False
        gridgen_quick_m = False
    
    # This is used if there is an existing mapping file for this foil
    # file and the outer radius. The mapping will not be changed.
    if gridgen_quick_n:
        np.load(fname2,'r')
    elif gridgen_quick_m:
        sio.loadmat(fname3,'r')
    else:
        # Read in airfoil coordinates (order might make a difference)
        mat = np.loadtxt(fname1)
        #for j in range(1,mat.shape[0]):
        #z(j) = mat[j-1:j,1] 

    # Save the variables into either an .npy or a .mat file
    if save_in_mat is None:
        save_in_mat = False
    elif save_in_mat=='T' or save_in_mat=='t':
        save_in_mat = True
    else:
        sys.stderr.write("Error: the third variable is not recognizable.")
        sys.exit()

    # Only for debugging purpose
    print(mat)
    print('gridgen_quick_n is ', gridgen_quick_n)
    print('gridgen_quick_m is ', gridgen_quick_m)
#    print('save_in_mat is ', save_in_mat)

    # End of program - calculate time!
    process_time = time.time() - start
    print(f'It took {process_time} seconds!')

gridgenmap('ncambre_20',1) # Only for debugging