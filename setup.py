from setuptools import setup, find_packages

setup(
    name = 'ScairPy',
    version = '0.0.1',
    description = 'SC Airfoil Toolbox exported from Matlab to Python',
    long_description = 'Read README',
    author = 'Mikihisa Yuasa',
    author_email='mikihisa.yuasa@wisc.edu',
    license='MIT',
    install_requires = [
        'numpy', 'pandas', 'scipy'
    ],
    zip_safe = False

)