# load_af_coords.py
#
# This gnerate coordinates required for GridgenArbitraryAirfoil.m from an
# arbitrary .dat file in Inputs/dat/. Also, an arbitrary number of nodes
# can be chosen.
#   
# *CAUTION*
#   the formats this functions supports are only those on UIUC database. If
#   other data format is an input, it just reads the data from the first
#   line.
#   Inputs
#       inpdat:     The name of input data file e.g. if there's a dat
#                   file called "Inputs/dat/NAME.dat", inpdatfile is the
#                   "NAME" part of the path. Format the file so that the
#                   data matrix starts from the 4th row and 1st column.
#       N:          The total nunmber of nodes. Less nodes make the 
#                   execution ofGridgenS.m faster. For the default mode,
#                   type either 'D', 'd', and the number of nodes will be
#                   automatically se as 10. If either 'O' or 'o' is typed,
#                   it returns the original number of nodes of the data
#                   file in anticlockwise without duplicate coords.
#                   
#   Outputs 
#                   foilfile: name of the output foilfile primarily for the
#                   later use on Gridgenmap.m or GridgenArbitraryAirfoil.m.
#                   Coordinate file in txt format. Ordered anticlockwise. 
#                   Located in Inputs/airfoils/coords/

import os 
import sys
import numpy as np
import pandas as pd
from return_coord_array import *

def load_af_coords(inpdat: str, N: int):

  # Load the input .dat file to a table(T) via a matrix(M).

  filename = '../inputs/dat/'+inpdat+'.dat'
  f = open(filename)
  tline1 = f.readline()
  tline2 = f.readline()
  tline3 = f.readline()
  tline4 = f.readline()
  f.seek(0,0)

  if tline3=='\n':
    A1 = np.loadtxt(f,skiprows=3)
    f.seek(0,0)
    line = f.readline()
    i=0
    while line:
      i = i + 1
      line = f.readline()
      if line=='\n':
        pos = i
    A = return_coord_array1(pos,f,A1)
    f.close()

  elif tline4.startswith(' 0'):
    A1 = np.loadtxt(f,skiprows=3)
    f.seek(0,0)
    line = f.readline()
    i=0
    while line:
      i = i + 1
      line = f.readline()
      if line==' \n':
        pos = i
    A = return_coord_array1(pos,f,A1)
    f.close()
  
  elif tline3.startswith('F'):
    A1 = np.loadtxt(f,skiprows=3)
    f.close()
    A = return_coord_array2(A1)

  elif tline2.startswith('   '):
    A1 = np.loadtxt(f,skiprows=1)
    f.close()
    A = return_coord_array2(A1)
  
  elif tline2.startswith('1'):
    A1 = np.loadtxt(f,skiprows=1)
    f.close()
    A = return_coord_array2(A1)
  
  else:
    print('The given data format is not supported on the UIUC database.'\
    + ' Reading from the very first line......')
    A1 = np.loadtxt(f)
    f.close()
    A = return_coord_array3(A1)

  # Select N nodes from the given data and create the final matrix of
  # the coords    

  H = A.shape[0]
  if N =='D' or N=='d':
    Nu = 10
  elif N =='O' or N=='o':
    Nu = H
  else:
    Nu = N 
  
  N = Nu

  if N>H:
    sys.stderr.write('Error: The given N is larger than the number of entire nodes.')
    sys.exit()
  
  # Chage the order of the coords so that the top coord comes first

  Hq = round(H/4)
  Hf = H-(Hq+3)
  Hs = H-(Hq+4)
  Af = A[Hf:H,:]
  As = A[0:Hs,:]
  A = np.r_[Af,As]

  a = H/N
  k = 0
  AF = np.empty((0,2))

  while k<N:
    mth_node = round(k*a)
    A_mth_row = A[mth_node:mth_node+1]
    AF = np.concatenate((AF,A_mth_row),axis=0)
    k = k + 1

  dirname = '../inputs/airfoils/coords/'
  name = dirname + inpdat + '_' + str(N) + '.txt'
  os.makedirs(dirname, exist_ok=True)
  with open(name, 'w') as f:
    np.savetxt(name,AF) # File output
  
  foilfile = inpdat + '_' + str(N) # Parameter output

  print('AF = ') # Terminal output
  print(' ')
  print(AF)
  
  return foilfile