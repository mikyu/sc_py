# retun_coord_array.py
#
# A set of functions returning an array of coordinates for load_af_coords

import numpy as np
import pandas as pd
import sys

def return_coord_array1(pos,f,A1):
    f.seek(0,0)
    Ab = np.loadtxt(f,skiprows=pos)
    H1 = A1.shape[0]
    Hb = Ab.shape[0]
    Hu = H1 - Hb
    Au = A1[0:Hu,:]
    Au = Au[Au[:,0].argsort()[::-1],:]
    A = np.r_[Ab,Au]
    AD = pd.DataFrame(A, columns=['x','y'])
    AD = AD.drop_duplicates()
    A = AD.values
    return A

def return_coord_array2(A1):
    H1 = A1.shape[0]
    if H1%2 == 0:
        Hu = H1/2
        Au = A1[0:Hu,:]
        Ab = A1[Hu:H1,:]
    elif H1%2 == 1:
        Hu = round(H1/2)
        Au = A1[0:Hu,:]
        Ab = A1[Hu:H1,:]
    else:
        sys.stderr.write('The number of coordinates is not an integer.')
    A = np.r_[Ab,Au]
    AD = pd.DataFrame(A, columns=['x','y'])
    AD = AD.drop_duplicates()
    A = AD.values
    return A

def return_coord_array3(A1):
    AD = pd.DataFrame(A1, columns=['x','y'])
    AD = AD.drop_duplicates()
    A = AD.values
    return A
